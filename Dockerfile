FROM node:14.18.1-alpine3.12 as development

WORKDIR /usr/src/app

COPY . .

RUN npm ci --only=development

RUN npm ci rimraf

RUN npm run build



FROM node:14.18.1-alpine3.12 as production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY --chown=node:node package*.json ./

RUN npm ci --only=production

COPY --chown=node:node --from=development /usr/src/app/dist ./dist

USER node

CMD ["node", "dist/main"]

EXPOSE ${SERVER_PORT}

