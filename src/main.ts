import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import appConfig from './common/config/app.config';
import helmet from 'helmet';
import { WsAdapter } from '@nestjs/platform-ws';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(helmet());
  app.enableCors();
  app.setGlobalPrefix('v1');
  app.useWebSocketAdapter(new WsAdapter(app));
  await app.listen(appConfig.serverPort);
}

bootstrap();
