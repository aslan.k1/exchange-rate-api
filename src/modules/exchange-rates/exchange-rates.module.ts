import { Module } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { ExchangeRatesRepository } from './data/exchange-rate.repository';
import { ExchangeRatesService } from './domain/exchange-rates.service';
import { IExchangeRatesRepository } from './domain/interfaces/exchange-rate-repository.interface';
import { IExchangeRatesService } from './domain/interfaces/exchange-rate-service.interface';
import { ExchangeRatesGateway } from './presenter/exchange-rate.gateway';
import { ExchangeRatesController } from './presenter/exchange-rates.controller';

@Module({
  imports: [AuthModule],
  controllers: [ExchangeRatesController],
  providers: [
    {
      provide: IExchangeRatesService,
      useClass: ExchangeRatesService,
    },
    {
      provide: IExchangeRatesRepository,
      useClass: ExchangeRatesRepository,
    },
    ExchangeRatesGateway,
  ],
  exports: [IExchangeRatesService],
})
export class ExchangeRatesModule {}
