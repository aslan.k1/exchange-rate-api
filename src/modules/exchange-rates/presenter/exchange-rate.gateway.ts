import { Inject, UseGuards, UseInterceptors } from '@nestjs/common';
import {
  MessageBody,
  WebSocketGateway,
  WebSocketServer,
  SubscribeMessage,
} from '@nestjs/websockets';
import { WsJwtGuard } from 'src/common/guards/jwt-ws.guard';
import { TransformInterceptor } from 'src/common/interceptors/transform.interceptor';
import { Server } from 'ws';
import { IExchangeRatesService } from '../domain/interfaces/exchange-rate-service.interface';

@UseGuards(WsJwtGuard)
@UseInterceptors(TransformInterceptor)
@WebSocketGateway({ path: '/exchange-rates' })
export class ExchangeRatesGateway {
  constructor(
    @Inject(IExchangeRatesService)
    private readonly exchangeRatesService: IExchangeRatesService,
  ) {}

  @WebSocketServer()
  server: Server;

  @SubscribeMessage('current')
  async findCurrent(@MessageBody() data: any): Promise<any> {
    const exchangeRate = await this.exchangeRatesService.findCurrentRates(
      data.base,
      data.symbols,
    );
    return { event: 'current', result: exchangeRate };
  }

  @SubscribeMessage('history')
  async findHistory(@MessageBody() data: any): Promise<any> {
    const exchangeRate = await this.exchangeRatesService.findHistoryRates(
      data.date,
      data.base,
      data.symbols,
    );
    return { event: 'history', result: exchangeRate };
  }
}
