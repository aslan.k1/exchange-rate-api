import {
  Controller,
  Get,
  Inject,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';
import { TransformInterceptor } from 'src/common/interceptors/transform.interceptor';
import { IExchangeRatesService } from '../domain/interfaces/exchange-rate-service.interface';

@UseGuards(JwtAuthGuard)
@UseInterceptors(TransformInterceptor)
@Controller('exchange-rates')
export class ExchangeRatesController {
  constructor(
    @Inject(IExchangeRatesService)
    private readonly exchangeRatesService: IExchangeRatesService,
  ) {}

  @Get('current')
  async findCurrent(
    @Query('base') base: string,
    @Query('symbols') symbols: string,
  ) {
    return this.exchangeRatesService.findCurrentRates(base, symbols);
  }

  @Get('history')
  async findHistory(
    @Query('date') date: Date,
    @Query('base') base: string,
    @Query('symbols') symbols: string,
  ) {
    return this.exchangeRatesService.findHistoryRates(date, base, symbols);
  }
}
