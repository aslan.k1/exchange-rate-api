import { Inject, Injectable } from '@nestjs/common';
import { IExchangeRatesService } from './interfaces/exchange-rate-service.interface';
import { IExchangeRatesRepository } from './interfaces/exchange-rate-repository.interface';
import { ExchangeRate } from './entities/exchange-rate.entity';

@Injectable()
export class ExchangeRatesService implements IExchangeRatesService {
  constructor(
    @Inject(IExchangeRatesRepository)
    private readonly exchangeRatesRepository: IExchangeRatesRepository,
  ) {}

  findCurrentRates(base: string, symbols: string): Promise<ExchangeRate> {
    return this.exchangeRatesRepository.findCurrent(base, symbols);
  }

  findHistoryRates(
    date: Date,
    base: string,
    symbols: string,
  ): Promise<ExchangeRate> {
    return this.exchangeRatesRepository.findHistory(date, base, symbols);
  }
}
