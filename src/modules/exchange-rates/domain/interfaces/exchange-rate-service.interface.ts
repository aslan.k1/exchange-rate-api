import { ExchangeRate } from '../entities/exchange-rate.entity';

export interface IExchangeRatesService {
  findCurrentRates(base: string, symbols: string): Promise<ExchangeRate>;
  findHistoryRates(
    date: Date,
    base: string,
    symbols: string,
  ): Promise<ExchangeRate>;
}

export const IExchangeRatesService = Symbol('IExchangeRatesService');
