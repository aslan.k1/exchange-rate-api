import { ExchangeRate } from '../entities/exchange-rate.entity';

export interface IExchangeRatesRepository {
  findCurrent(base: string, symbols: string): Promise<ExchangeRate>;
  findHistory(date: Date, base: string, symbols: string): Promise<ExchangeRate>;
}

export const IExchangeRatesRepository = Symbol('IExchangeRatesRepository');
