export class ExchangeRate {
  date: Date;
  base: string;
  rates: Map<string, any>;
}
