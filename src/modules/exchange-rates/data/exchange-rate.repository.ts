import { IExchangeRatesRepository } from '../domain/interfaces/exchange-rate-repository.interface';
import axios from 'axios';
import appConfig from 'src/common/config/app.config';
import { ExchangeRate } from '../domain/entities/exchange-rate.entity';
import { HttpException } from '@nestjs/common';

export class ExchangeRatesRepository implements IExchangeRatesRepository {
  async findCurrent(base: string, symbols: string): Promise<ExchangeRate> {
    const result = await axios.get(
      `https://api.apilayer.com/fixer/latest?symbols=${symbols}&base=${base}`,
      {
        headers: {
          apikey: appConfig.apiLayerApiKey,
        },
      },
    );

    if (!result.data.success) {
      throw new HttpException(result.data.error.type, result.data.error.code);
    }

    return this.toExchangeRate(result.data);
  }

  async findHistory(
    date: Date,
    base: string,
    symbols: string,
  ): Promise<ExchangeRate> {
    const result = await axios.get(
      `https://api.apilayer.com/fixer/${date}?symbols=${symbols}&base=${base}`,
      {
        headers: {
          apikey: appConfig.apiLayerApiKey,
        },
      },
    );

    if (!result.data.success) {
      throw new HttpException(result.data.error.type, result.data.error.code);
    }

    return this.toExchangeRate(result.data);
  }

  private toExchangeRate(data: any): ExchangeRate {
    return {
      date: data.date,
      base: data.base,
      rates: new Map(Object.entries(data.rates)),
    };
  }
}
