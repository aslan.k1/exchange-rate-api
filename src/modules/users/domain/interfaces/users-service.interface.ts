import { User } from '../entities/user.entity';

export interface IUsersService {
  findOneById(id: number): Promise<User>;
  findOneByEmail(email: string): Promise<User>;
}

export const IUsersService = Symbol('IUsersService');
