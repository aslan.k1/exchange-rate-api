import { User } from '../entities/user.entity';

export interface IUsersRepository {
  findById(id: number): Promise<User>;
  findByEmail(email: string): Promise<User>;
}

export const IUsersRepository = Symbol('IUsersRepository');
