import { TEST_USER } from 'src/common/constants/test-user.const';
import { IUsersRepository } from '../domain/interfaces/users-repository.interface';
import { User } from '../domain/entities/user.entity';

export class FakeUsersRepository implements IUsersRepository {
  users: User[] = [TEST_USER];

  async findById(id: number): Promise<User> {
    return this.users.find((user) => user.id === id);
  }
  async findByEmail(email: string): Promise<User> {
    return this.users.find((user) => user.email === email);
  }
}
