import { Module } from '@nestjs/common';
import { FakeUsersRepository } from './data/fake-users-repository';
import { IUsersRepository } from './domain/interfaces/users-repository.interface';
import { IUsersService } from './domain/interfaces/users-service.interface';
import { UsersService } from './domain/users.service';

@Module({
  controllers: [],
  providers: [
    {
      provide: IUsersRepository,
      useClass: FakeUsersRepository,
    },
    {
      provide: IUsersService,
      useClass: UsersService,
    },
  ],
  exports: [IUsersService],
})
export class UsersModule {}
