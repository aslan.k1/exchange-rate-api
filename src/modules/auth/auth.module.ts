import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import appConfig from 'src/common/config/app.config';
import { UsersModule } from '../users/users.module';
import { AuthService } from './domain/auth.service';
import { IAuthService } from './domain/interfaces/auth-service.interface';
import { JwtStrategy } from './domain/strategies/jwt.strategy';
import { AuthController } from './presenter/auth.controller';

@Module({
  imports: [
    PassportModule,
    UsersModule,
    JwtModule.register({
      secret: appConfig.jwt.secret,
      signOptions: { expiresIn: appConfig.jwt.expiresIn },
    }),
  ],
  controllers: [AuthController],
  providers: [
    {
      provide: IAuthService,
      useClass: AuthService,
    },
    JwtStrategy,
  ],
  exports: [IAuthService],
})
export class AuthModule {}
