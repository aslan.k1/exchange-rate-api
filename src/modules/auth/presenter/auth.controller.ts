import {
  Controller,
  Post,
  Body,
  ValidationPipe,
  UseInterceptors,
  Inject,
} from '@nestjs/common';
import { TransformInterceptor } from 'src/common/interceptors/transform.interceptor';
import { IAuthService } from '../domain/interfaces/auth-service.interface';
import { LoginDto } from './dto/login-dto';

@UseInterceptors(TransformInterceptor)
@Controller('auth')
export class AuthController {
  constructor(
    @Inject(IAuthService) private readonly authService: IAuthService,
  ) {}

  @Post('login')
  async login(@Body(ValidationPipe) loginDto: LoginDto) {
    return this.authService.login(loginDto);
  }
}
