import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { IUsersService } from 'src/modules/users/domain/interfaces/users-service.interface';
import { LoginDto } from '../presenter/dto/login-dto';
import * as bcrypt from 'bcryptjs';
import { IAuthService } from './interfaces/auth-service.interface';

@Injectable()
export class AuthService implements IAuthService {
  constructor(
    @Inject(IUsersService)
    private readonly usersService: IUsersService,
    private readonly jwtService: JwtService,
  ) {}

  async login(loginDto: LoginDto): Promise<any> {
    const user = await this.usersService.findOneByEmail(loginDto.email);

    await this.checkPassword(loginDto.password, user.password);

    const payload = {
      id: user.id,
      email: user.email,
    };

    return {
      accessToken: this.jwtService.sign(payload),
      user: payload,
    };
  }

  async validateUser(payload: any) {
    const userId = payload.id;
    const user = await this.usersService.findOneById(userId);

    return user;
  }

  private async checkPassword(password: string, hashPassword: string) {
    const passwordsMatch = await bcrypt.compare(password, hashPassword);
    if (!passwordsMatch) {
      throw new UnauthorizedException('Credentials are invalid');
    }
  }
}
