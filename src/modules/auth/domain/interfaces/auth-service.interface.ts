import { User } from 'src/modules/users/domain/entities/user.entity';
import { LoginDto } from '../../presenter/dto/login-dto';

export interface IAuthService {
  login(loginDto: LoginDto): Promise<any>;
  validateUser(payload: any): Promise<User>;
}

export const IAuthService = Symbol('IAuthService');
