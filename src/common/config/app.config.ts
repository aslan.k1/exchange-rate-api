import dotenv from 'dotenv';

dotenv.config();

export default {
  serverPort: +process.env.SERVER_PORT,
  jwt: {
    secret: process.env.JWT_SECRET,
    expiresIn: '7d',
  },
  apiLayerApiKey: process.env.API_LAYER_API_KEY,
};
