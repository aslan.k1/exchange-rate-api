import {
  Injectable,
  CanActivate,
  ExecutionContext,
  Inject,
} from '@nestjs/common';
import { User } from 'src/modules/users/domain/entities/user.entity';
import * as jwt from 'jsonwebtoken';
import appConfig from '../config/app.config';
import { WsException } from '@nestjs/websockets';
import { IAuthService } from 'src/modules/auth/domain/interfaces/auth-service.interface';

@Injectable()
export class WsJwtGuard implements CanActivate {
  constructor(
    @Inject(IAuthService) private readonly authService: IAuthService,
  ) {}

  async canActivate(context: ExecutionContext) {
    try {
      const data = context.switchToWs().getData();
      const authHeader = data.headers.authorization;
      const authToken = authHeader.substring(7, authHeader.length);
      const jwtPayload: any = <any>jwt.verify(authToken, appConfig.jwt.secret);
      const user: User = await this.authService.validateUser(jwtPayload);
      // Bonus if you need to access your user after the guard
      context.switchToWs().getData().user = user;

      return Boolean(user);
    } catch (err) {
      throw new WsException(err.message);
    }
  }
}
