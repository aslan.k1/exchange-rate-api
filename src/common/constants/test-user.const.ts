import { User } from 'src/modules/users/domain/entities/user.entity';

// Email: test@example.com
// Password: test
export const TEST_USER: User = {
  id: 1,
  email: 'test@example.com',
  password: '$2b$10$XNE7DvmYHOJeLbGinInsZ.xdp3KlqGTfJBUEofe/Ph8/XjA8Z3gJ6',
};
