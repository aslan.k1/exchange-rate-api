# Exchange Rate API

API Service of Exchange Rate Project

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Quick documentation

```bash
# Test user credentials
Email: test@example.com
Password: test

# REST API
POST /v1/auth/login
Return access token and user.
Request body:
email : string (required) - The email of a user
password : string (required) - The password of user


GET /v1/exchange-rates/current 
Returns current exchange rate
Params: 
base : string (optional) - The three-letter currency code of preferred base currency
symbols : string (optional) - A list of comma-separated currency codes to limit output currencies


GET /v1/exchange-rates/history
Return historical rates by a date
Params: 
date : string (required) - A date in the past for which historical rates are requested
base : string (optional) - The three-letter currency code of your preferred base currency
symbols : string (optional) - A list of comma-separated currency codes to limit output currencies

# Websocket API
GET ws: host:port/exchange-rates/

Request body for a current event:
    event: 'current',
    data: {
        headers: {
            authorization: `Bearer ${jwtToken}`,
        },
        base: 'USD',
        symbols: 'EUR',
    },

Request body for a history event:
    event: 'current',
    data: {
        headers: {
            authorization: `Bearer ${jwtToken}`,
        },
        date: '2014-06-22',
        base: 'USD',
        symbols: 'EUR',
    },
```